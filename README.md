###Project Name
##### **Codeigniter_CRUD**
###Description
This project build in Codeigniter.CRUD stand for Create Read Update Delete.
codeigniter is an Application Development Framework - a toolkit - for people who build web sites using PHP. 
Its goal is to enable you to develop projects much faster than you could if you were writing code from scratch,
by providing a rich set of libraries for commonly needed tasks, as well as a simple interface and logical structure
to access these libraries. CodeIgniter lets you creatively focus on your project by minimizing the amount of code needed
for a given task.
### Installation
#####**To install the codeigniter  follow the link below**
[ ]**https://codeigniter.com/**
#####**Step2:After downloading Codeigniter place it in htdocs folder.**
#####**Step3:Create database Ci_test  using mysql or sqlserver**
### Features
#####**In CRUD insert,update, view and delete operations performed.**
### Author
#####**M.Sohaib**
### Copyright
#####**IT Serve Group @ 2018.All Rights Reserved.**
### Reference
#####**IT Serve Group**