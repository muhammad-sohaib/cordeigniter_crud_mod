<!DOCTYPE>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<title>CRUD All_Record</title>
<style>
 h2{
	text-align: center;
	font-size: 24px;
        margin-top: 5%;
        color: #2BB9EC;
}

#submit {
	font-size: 12px;
	background: linear-gradient(#005BB1, #001824);
	border: 1px solid #0F799E;
	padding: 7px 25px;
	color: white;
	text-shadow: 0px 1px 0px #13506D;
	font-weight: bold;
	border-radius: 2px;
	cursor: pointer;
	width: 100%;
}
#submit:hover{
    background:#00B312;
}
.btn {
	font-size: 16px;
	background: linear-gradient(#005BB1, #001824);
	border: 1px solid #0F799E;
	padding: 1% 4%;
        margin-top:4%;
	color:white;
	text-shadow: 0px 1px 0px #13506D;
	font-weight: bold;
	border-radius: 4px;
	cursor: pointer;
	width: 20%;
}
.btn:hover{
    background:#00B312;
}

table{
    margin-top:4%;
    background: linear-gradient(#005BB1, #001824);
    
}
table th{
    color:white;
    background-color:#4A2600;
}
table td{
    color:black;
}


</style>
<script type="text/javascript">
function show_confirm(act,gotoid)
{
if(act=="edit")
var r=confirm("Do you really want to edit?");
else
var r=confirm("Do you really want to delete?");
if (r==true)
{
window.location="<?php echo base_url(); ?>users/"+act+"/"+gotoid;

}
}
</script>
</head>
<body>
<div class="container">    
<h2> CRUD All_Record </h2>
<center>
<table class="table">
<tr>  
<th class="thead">ID</th>
<th class="thead">User Name</th>
<th class="thead">Email</th>
<th class="thead">Address</th>
<th class="thead">Mobile</th>
<th class="thead"  colspan="2">Action</th>
</tr>
<?php foreach ($user_list as $u_key){ ?>
<tr>
<td><?php echo $u_key->id; ?></td>
<td><?php echo $u_key->name; ?></td>
<td><?php echo $u_key->email; ?></td>
<td><?php echo $u_key->address; ?></td>
<td><?php echo $u_key->mobile; ?></td>
<td width="40" align="left" ><a href="#" onClick="show_confirm('edit',<?php echo $u_key->id;?>)"><input type="submit" id="submit" name="submit" value="Edit" /></a></td>
<td width="40" align="left" ><a href="#" onClick="show_confirm('delete',<?php echo $u_key->id;?>)"><input type="submit" id="submit" name="submit" value="Delete" /> </a></td>
</tr>
<?php }?>
<tr>
 <td colspan="7" align="center"> <a  style="color:#2BB9EC;" href="users/add_form"><button type="button"class="btn"> Insert new record </button></a></td>
</tr>
</table>
</center>    
</div>     
</body>
</html>

