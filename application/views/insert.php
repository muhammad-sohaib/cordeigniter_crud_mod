<!DOCTYPE>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Insert Form</title>
<style>
  h2{
	text-align: center;
	font-size: 24px;
        color: white;
}
hr{
	margin-bottom: 30px;
}
div.container{
	width: 300px;
	height: 600px;
	margin:50px auto;
	font-family: 'Droid Serif', serif;
	position:relative;
}
div.main{
	width: 300px;
	float:left;
	padding: 5px 50px 25px;
	background: linear-gradient(#4A2600, #61210B,#752B02);
	border: 8px solid #61210B;
	box-shadow: 0 0 10px;
	border-radius: 2px;
	font-size: 13px;
}
input[type=text] {
	width: 100%;
	height: 34px;
	padding-left: 5px;
	margin-bottom: 20px;
	margin-top: 8px; 
        background-color: #6D544F;
	box-shadow: 0 0 5px white;
	border: 1px solid #752B02;
	color: white;
	font-size: 20px;
}
.Address {
	width: 100%;
	height: 15%;
	padding-left: 5px;
	margin-bottom: 20px;
	margin-top: 8px;
        background-color:#6D544F;
	box-shadow: 0 0 5px white;
	border: 1px solid  #752B02;
	color: white;
	font-size: 20px;
}
#submit {
	font-size: 20px;
	margin-top: 15px;
	background: linear-gradient(#4A2600, #61210B);
	border: 1px solid #6D544F;
	padding: 7px 35px;
	color: white;
	text-shadow: 0px 1px 0px #13506D;
	font-weight: bold;
	border-radius: 2px;
	cursor: pointer;
	width: 100%;
}
#submit:hover{
   background: linear-gradient(#005BB1, #001824);
}
.fugo{
	float:right;
}

   
</style>
</head>
<body>
<div class="container">
<div class="main">
<form method="post" action="<?php echo base_url('index.php/users/insert_new_user'); ?>">    
<h2>User_Form</h2>
<input type="text"  name="name" placeholder="Full_Name"/>
<input type="text"  name="email" placeholder="Email_Address"/>
<input type="text"  name="mobile" placeholder="Contact_No"/>
<textarea class="Address" name="address" placeholder="Address" rows="5" cols="20"></textarea>
<input type="submit" id="submit" name="submit" value="Send" />
</form>
</div>
</div>
</body>
</html>
